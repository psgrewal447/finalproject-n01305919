﻿<%@ Page Title="index" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="crud_n01305919.index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
 <center>
<!--reference from https://forums.asp.net/t/1015092.aspx?SQLdatasource+gridview+mysql -->
<asp:FormView ID="Crud_form" runat="server" AllowPaging="True" DataKeyNames="Page_title" DataSourceID="SqlDataSource1" DefaultMode="Insert" Height="129px" Width="608px">
<EditItemTemplate>
Id:<asp:TextBox ID="entry_id" runat="server" Text='<%#Bind("Id")%>'/>
 <br/>
Page_title:<asp:Label ID="Title_page" runat="server" Text='<%#Eval("Page_title")%>'/>
 <br/>
Description:<asp:TextBox ID="Description" runat="server" Text='<%#Bind("Description")%>'/>
 <br/>
 <asp:LinkButton ID="UpdateButton" runat="server" CommandName="Update" CausesValidation="True"  Text="Update"/>
 <asp:LinkButton ID="UpdateCancelButton" runat="server"  CommandName="Cancel" CausesValidation="False" Text="Cancel"/>
 </EditItemTemplate>
 <InsertItemTemplate>
<strong><em>Id:</em></strong><asp:TextBox ID="entry_Id" runat="server" Text='<%#Bind("Id")%>'/>
 <br/>
<strong><em>Page_title:</em></strong><asp:TextBox ID="Title_page" runat="server" Text='<%#Bind("Page_title")%>'/>
 <br/>
<strong><em>Description:</em></strong><asp:TextBox ID="Description" runat="server" Text='<%#Bind("Description")%>'/>
 <br/>
 <br/>
 <asp:LinkButton ID="InsertBtn" runat="server"  CommandName="Insert" CausesValidation="True" Text="Add-Row"/>
 <asp:LinkButton ID="InsertCancelButton" runat="server"  CausesValidation="False" CommandName="Cancel" Text="Clear"/>
</InsertItemTemplate>
 <ItemTemplate>
  Id:<asp:Label ID="entry_Id" runat="server" Text='<%#Bind("Id")%>'/>
 <br/>
 Page_title:<asp:Label ID="title_page" runat="server" Text='<%#Eval("Page_title")%>'/>
 <br/>
Description:<asp:Label ID="Description" runat="server" Text='<%#Bind("Description")%>'/>
 <br/><asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" CausesValidation="False"  Text="Edit"/>
 <asp:LinkButton ID="DeleteButton" runat="server"  CommandName="Delete" Text="Delete" CausesValidation="False"/>
 <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" Text="New" CommandName="New"/>
 </ItemTemplate>
 </asp:FormView>
 <br/>
     <!--only some parts refer from https://stackoverflow.com/questions/7122929/gridview-sortedascendingcellstyle-server-side-databinding -->
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [Table] WHERE [Page_title] = @Page_title" InsertCommand="INSERT INTO [Table] ([Id], [Page_title], [Description]) VALUES (@Id, @Page_title, @Description)" SelectCommand="SELECT * FROM [Table]" UpdateCommand="UPDATE [Table] SET [Id] = @Id, [Description] = @Description WHERE [Page_title] = @Page_title">
<DeleteParameters>
 <asp:Parameter Name="Page_title" Type="String"/>
</DeleteParameters>
 <InsertParameters>
<asp:Parameter Name="Id" Type="Int32"/>
<asp:Parameter Name="Page_title" Type="String"/>
<asp:Parameter Name="Description" Type="String"/>
</InsertParameters>
<UpdateParameters>
<asp:Parameter Name="Id" Type="Int32"/>
<asp:Parameter Name="Description" Type="String"/>
<asp:Parameter Name="Page_title" Type="String"/>
</UpdateParameters>
</asp:SqlDataSource>
<br/><br/>
 <asp:GridView ID="main_GridView" runat="server" AllowSorting="True"  BackColor="#CCCCCC" BorderColor="#999999" AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" DataKeyNames="Page_title" DataSourceID="SqlDataSource1" ForeColor="Black" Height="205px" Width="860px">
 <Columns>
 <asp:CommandField ShowDeleteButton="True" ShowEditButton="True"/>
 <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id"/>
 <asp:BoundField DataField="Page_title" HeaderText="Page_title" ReadOnly="True" SortExpression="Page_title"/>
 <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description"/>
 </Columns>
<FooterStyle BackColor="#CCCCCC"/>
<HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White"/>
<PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left"/>
<RowStyle BackColor="White"/>
<SelectedRowStyle BackColor="#000099" ForeColor="White" Font-Bold="True"/>
<SortedAscendingCellStyle BackColor="#F1F1F1"/>
<SortedAscendingHeaderStyle BackColor="#808080"/>
<SortedDescendingCellStyle BackColor="#CAC9C9"/>
<SortedDescendingHeaderStyle BackColor="#383838"/>
</asp:GridView>
 <br/><br/>

</asp:Content>
